extends Position2D
export (int) var path_amount
var triggered = false 

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _physics_process(delta):
	
	if triggered:
		get_parent().set_offset(get_parent().get_offset() + (path_amount * delta)) #in pixels
