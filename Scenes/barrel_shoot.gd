extends Area2D
signal shoot
export (PackedScene) var Bullet3
export (float) var fire_rate
var can_shoot = true
var player_in_spot = false
var destroy_leftovers = false 

var is_alive = true 
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$GunTimer.wait_time = fire_rate
	$Enemy3Anim.play("idle")
	


func _process(delta):
	
	if destroy_leftovers:
		utilize()

	if can_shoot and player_in_spot:
		print("shooting")
		shoot()

func _on_SawPlayer_body_entered(body):
	if body.is_in_group("Player"):
		player_in_spot = true

func _on_SawPlayer_body_exited(body):
	if body.is_in_group("Player"):
		player_in_spot = false

func _on_GunTimer_timeout():
	can_shoot = true 

func shoot():
	
	emit_signal("shoot", Bullet3, $Muzzle.position, rotation)
	can_shoot = false
	$GunTimer.start()

func update_score():
	Global.score += 140 


func Explosion():
	Explode()

func Explode():
	update_score()
	get_node("CollisionShape2D").queue_free()
	$Enemy3Anim.play("explode")
	$SawPlayer.queue_free()
	$DestroyTimer.start()

func _on_DestroyTimer_timeout():
	destroy_leftovers = true
	
func utilize():
	get_parent().queue_free()
	

func _on_Area2D_barrel_shoot(bullet, pos, dir):
	var b = bullet.instance()
	b.start(pos, dir)
	get_tree().get_root().get_node("Main").Play_Enemy_Barrel_Sound()

	get_parent().get_parent().add_child(b)
	b.global_position.x = get_parent().position.x 
	b.global_position.y = get_parent().position.y + 30 

