extends AnimatedSprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _on_enemy1_anim_animation_finished():
	
	print("playing explode animation")
	$enemy1_anim.stop()
	$enemy1_anim.hide()
	get_parent().queue_free()