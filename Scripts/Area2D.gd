extends Area2D

var curr_pos
var move_dir
var x = 0
var h
var T 
var N 
var step_x
var arg = 0
var current_animation
var is_alive = true

var target = Vector2()
var velocity = Vector2()
var speed = 50
var break_away 
var aim_player = false
var speed_aim = 0
var player_aimed = false
var player_pos = Vector2()


var in_block = true 
export var path_amount = 50


var RotateSpeed = 0
var Radius = 0
var _centre
var _angle = 0
var side

var direction
var motion 
var respawned = false

var dir
var last_pos

var pos_thresh = 5
var _timer
var destroy_leftovers = false 
var break_away_sound = true 
var node_name 
var break_away_finished = false 



func _ready():
	$enemy1_anim.play("idle")
	$DestroyTimer.wait_time = 2
	node_name = get_parent().name
	
func _physics_process(delta):
	
	if in_block:
		break_away_sound = true
	

	if global_position.y > 1600:
		respawn()
		
	if global_position.y > 1200:
		get_parent().outside_screen = true #start moving like in block 

	if respawned and not(in_block):
		get_to_block(delta)
		
	if RotateSpeed == 0:
		RotateSpeed = get_parent().RotateSpeed 
		
	if Radius == 0:
		Radius = get_parent().Radius
		
	if speed_aim == 0:
		speed_aim = get_parent().speed_aim
	
	
	break_away = get_parent().break_away

	if break_away:
		move_away(delta)
		
		if break_away_sound:
			get_tree().get_root().get_node("Main").play_break_away_sound()
			break_away_sound = false 
		
		if not(player_aimed):
			player_pos = read_player_pos()
			player_aimed = true
			
	if aim_player and player_aimed:
		move_to_player(player_pos, delta)
		
		
	if destroy_leftovers:
		utilize()
		
		
func respawn():
	
	

	#get_parent().in_block = true
	get_parent().aim_player = false

	get_parent().break_away = false
	break_away = false
	
	aim_player = false 
	player_aimed = false
	_angle = 0 
	
	global_position = Vector2(500,50) 
	respawned = true
	
	
func get_to_block(delta):
	
	
	if abs(global_position.x - get_parent().position.x) > pos_thresh and abs(global_position.y - get_parent().position.y) > pos_thresh:
		dir = (get_parent().pos_vector - global_position).normalized()
		var motion2 = dir * 400 * delta
		global_position += motion2
	else:
		get_parent().in_block = true
		respawned = false 
		
		in_block = true 
		position = Vector2(0,0)
		get_parent().pickable = true 
		get_parent().aim_player = false
		get_parent().outside_screen = false 

func Explosion():
	Explode()

func Explode():
	update_score()
	get_parent().get_parent().destroyed_childs += 1 
	$enemy1_anim.play("explode")
	get_node("CollisionShape2D").queue_free()
	$DestroyTimer.start()


func update_score():
	Global.score += 30
	
	
func utilize():
	
	get_parent().queue_free()
	
func read_player_pos():
		return Global.player_pos 

func move_to_player(target, delta):

	
	if global_position.y < 930:
		direction = (target - global_position).normalized()
		motion = direction * speed_aim * delta
		global_position += motion
		
	else:
		motion = direction * speed_aim * delta
		global_position += motion


func move_away(delta): 

	
	get_parent().in_block = false
	in_block = false
	
	_centre = self.global_position
	_angle += RotateSpeed * delta;
	
	if position.x > 960:

		if _angle < PI:
			var offset = Vector2(-cos(_angle), sin(_angle) ) * Radius;
			var pos = _centre + offset
			self.global_position = pos
			
		else:
			get_parent().break_away = false 
			aim_player = true
			
	else:
		if _angle < PI:
			var offset = Vector2(sin(_angle), -cos(_angle) ) * Radius;
			var pos = _centre + offset
			self.global_position = pos
			
		else:
			get_parent().break_away = false 
			aim_player = true
			



func _on_DestroyTimer_timeout():
	destroy_leftovers = true
