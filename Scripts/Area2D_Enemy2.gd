extends Area2D
var is_alive = true 
signal shoot
export (PackedScene) var Bullet2
export (float) var fire_rate
var can_shoot = false
var player_in_spot = false
var destroy_leftovers = false 

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$GunTimer.wait_time = fire_rate


func _process(delta):
	if can_shoot and player_in_spot:
		shoot()
		
	if destroy_leftovers:
		utilize()
		
		
	
func shoot():
	emit_signal("shoot", Bullet2, $Muzzle.position, rotation)
	can_shoot = false
	$GunTimer.start()


func _on_Area2DEnemy2_shoot(bullet, pos, dir):
	var b = bullet.instance()
	b.start(pos, dir)
	get_tree().get_root().get_node("Main").play_enemy_shoot_sound()
	add_child(b)

func Explosion():
	Explode()

func Explode():
	update_score()
	get_node("CollisionShape2D").queue_free()
	$Enemy2Anim.play("explode")
	$SawPlayer.queue_free()
	$DestroyTimer.start()

func update_score():
	Global.score += 120 

func _on_GunTimer_timeout():
	can_shoot = true
	
func _on_SawPlayer_body_entered(body):
	if body.is_in_group("Player"):
		player_in_spot = true

func _on_SawPlayer_body_exited(body):
	if body.is_in_group("Player"):
		player_in_spot = false
		
func _on_DestroyTimer_timeout():
	destroy_leftovers = true
	
	
func utilize():
	get_parent().queue_free()

