extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$wskazowka.rotation_degrees = -56 
	pass

func _process(delta):
	if Global.player_lives == 2:
		$wskazowka.rotation_degrees = -33
		
	if Global.player_lives == 1:
		$wskazowka.rotation_degrees = 13.8
	
	if Global.player_lives == 0:
		$wskazowka.rotation_degrees = 60 
	
