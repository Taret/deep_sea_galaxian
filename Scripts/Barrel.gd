
extends Node2D

export (int) var speed 
var velocity = Vector2()

func _ready():
	pass


func _process(delta):
	
	if global_position.y > 1100:
		pollution_up()
		queue_free() 
	
	
	global_position += velocity * delta


func start(pos, dir):
	position = pos
	rotation = dir 
	velocity = Vector2(0, speed).rotated(dir)
	
	
func destroyed_by_players_bullet():
	pollution_up()
	get_tree().get_root().get_node("Main").Play_barrel_explosion()
#	$Area2D/AnimatedSprite.play("explode")
	$DestroyTimer.start()
	


func _on_Area2D_body_entered(body):
	
	if body.is_in_group('Player') and body.floodgate_open:
		get_tree().get_root().get_node("Main").Play_Barrel_Catched()
		queue_free()
	
	if body.is_in_group('Player') and body.can_take_dmg and body.floodgate_open == false :
		Global.player_lives -= 1
		body.took_dmg = true 
		pollution_up()
		Global.score = 0 
		queue_free()
		
	
		
func pollution_up():
	get_tree().get_root().get_node("Main").pollution += 1 


func _on_DestroyTimer_timeout():
	utilize()

func utilize():
	queue_free()