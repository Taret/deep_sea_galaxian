
extends Area2D

export (int) var speed 
var velocity = Vector2()

func _ready():
	$AudioStreamPlayer2D.play()


func start(pos, dir):
	position = pos
	rotation = dir 
	velocity = Vector2(0, -speed).rotated(dir)


func _process(delta):
	position += velocity * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free() 
			
func _on_Bullet_area_entered(area):
	if area.is_in_group('Enemy'):
		area.is_alive = false
		area.Explosion()
		
		get_tree().get_root().get_node("Main").play_explode_sound()
		queue_free()
		
	elif area.is_in_group("Barrel"):
		area.get_parent().destroyed_by_players_bullet()
		
		

		
		
	

		
