
extends Area2D

export (int) var speed 
var velocity = Vector2()

func _ready():
	pass


func _process(delta):
	
	
	
	if global_position.y > 1100:
		queue_free()
	
	position += velocity * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free() 
			

func start(pos, dir):
	position = pos
	rotation = dir 
	velocity = Vector2(0, speed).rotated(dir)
	




func _on_Bullet_body_entered(body):
	if body.is_in_group('Player') and body.can_take_dmg:
		Global.player_lives -= 1
		body.took_dmg = true 
		


