extends KinematicBody2D


func _physics_process(delta):
	#move towards target destination
	
	
	#once target destination is reached
#	if (get_pos().distance_to(target) <= 20):
	
	var target = Vector2(980, 980)
	var speed = 20 
	var angle = get_angle_to(target)		#calc angle to target
	var velocity = Vector2(speed*sin(angle), speed*cos(angle))			#convert linear speed into x and y components
	
	move_and_collide(velocity)