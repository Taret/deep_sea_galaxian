extends Node2D
var main_enemies_updated = false 


# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	
	pass

func _process(delta):
	
	if get_child_count() == 0 and not(main_enemies_updated):
		update_enemies_destroyed()

func update_enemies_destroyed():
	get_tree().get_root().get_node("Main").destroyed_enemies += 1 
	main_enemies_updated = true 