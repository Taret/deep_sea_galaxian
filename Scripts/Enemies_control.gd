extends Node2D

var can_pick = true 
var _timer
export (float) var wait_time
export (int) var T 
export (int) var N 
export (int) var speed_block 


export (int) var RotateSpeed 
export (int) var Radius 

export (int) var speed_aim
var main_enemies_updated = false 

export (int) var trigger_enemies2 
var destroyed_childs = 0 

func _ready():
	_timer = get_parent().get_child(0)
	_timer.wait_time = wait_time
	
func _process(delta):
	
	if destroyed_childs >= trigger_enemies2:
		trigger_enem2()
	
	
	if get_child_count() == 0 and not(main_enemies_updated):
		update_enemies_destroyed()
		
	
	pick_enemy()
	

func pick_enemy():
	
	var childs = get_child_count()

	if childs > 0:
	
		if can_pick: 
	
			randomize()
			var id = randi() % childs 
			
			if id < 0:
				id *= -1
			
			var enemy = get_child(id)
			var pickable = enemy.pickable
			
					
			
			
			
			if pickable:
				
				_timer.start()
	#			
				#get_tree().get_root().get_node("Main").get_child(2).get_child(0).play()
				enemy.break_away = true
				enemy.in_block = false
				can_pick = false 
		
func _on_EnemyTimer_timeout():
	can_pick = true
	
func update_enemies_destroyed():
	get_tree().get_root().get_node("Main").destroyed_enemies += 1 
	main_enemies_updated = true 
	
func trigger_enem2():
	get_tree().get_root().get_node("Main/CanvasLayer/lvl_control/Enemies2").triggered = true 
	get_tree().get_root().get_node("Main/CanvasLayer/lvl_control/Enemies2_2").triggered = true 
