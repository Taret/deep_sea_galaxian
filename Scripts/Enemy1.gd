extends Node2D

export (int) var T = 0
export (int) var N = 0
var is_alive 

const move_dir = 1 
var step_x 
var h
var arg = 0
var curr_pos
var amp = 5
var target = Vector2()
var aim_player 
var in_block 
var break_away

var speed_block = 0 

var RotateSpeed = 0
var Radius = 0

var speed_aim = 0
var last_pos = Vector2() #stores in-time position in block
var curr_in_block_pos = Vector2()

var move_dir_temp = 1 

var t1 = 0
var pos_vector = Vector2()
var pickable = true
var outside_screen = false 

	
func _ready():
	
	if T == 0:
		T = get_parent().T
	
	if N == 0:
		N = get_parent().N
		
	if speed_block == 0:
		speed_block = get_parent().speed_block
		
	if RotateSpeed == 0:
		RotateSpeed = get_parent().RotateSpeed
		
	if Radius == 0:
		Radius = get_parent().Radius 
		
	if speed_aim == 0:
		speed_aim = get_parent().speed_aim

	pos_vector = position #read initial enemy position as a starting point for sin 

	in_block = true
	break_away = false 

func _physics_process(delta):
	
	harmonic_movement(delta)
	
	if in_block and not(aim_player) or outside_screen:
		move_block(delta)
		
	if break_away and not(aim_player):
		$Area2D.break_away = break_away
		pickable = false 


func move_block(delta):

	move_dir = Global.block_move_dir

	
	position.x = pos_vector.x 
	position.y = pos_vector.y

		

func harmonic_movement(delta):
	
	
	
	move_dir = Global.block_move_dir
	step_x = (T*0.5)/2 #calculate increment at x up to pi/2
	h = step_x/N #x step
	arg += PI/2/N
	
	if pos_vector.x > 1620:
		Global.block_move_dir *= -1 
		pos_vector.x -= 10 
	elif pos_vector.x < 300:
		Global.block_move_dir *= -1 
		pos_vector.x += 10 

	pos_vector.x += (h * move_dir * delta)*speed_block 
	pos_vector.y += (cos(arg) * delta)*speed_block
	
#	if pos_vector.x > 1620:
#		pos_vector.x -= (h * move_dir * delta)*speed_block
#		pos_vector.y += (cos(arg) * delta)*speed_block
#	elif pos_vector.x < 320:
#		pos_vector.x += (h * move_dir * delta)*speed_block
#		pos_vector.y += (cos(arg) * delta)*speed_block
	

func _on_Area2D_body_entered(body):
	
	if body.is_in_group("Player"):
		Global.player_lives -= 1
		body.took_dmg = true
		$Area2D.Explode()
		get_tree().get_root().get_node("Main").play_explode_sound()
		
	if body.is_in_group("Constraint") and in_block:
		Global.block_move_dir *= -1
		
