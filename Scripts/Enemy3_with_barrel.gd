extends Node2D

export (int) var T = 200
export (int) var N = 20
var is_alive 

const move_dir = 1 
var step_x 
var h
var arg = 0
var curr_pos
var amp = 5
var target = Vector2()
var aim_player 
var in_block 
var break_away

var speed_block = 0 
var RotateSpeed = 0
var Radius = 0

var speed_aim = 0
var last_pos = Vector2() #stores in-time position in block
var curr_in_block_pos = Vector2()

var move_dir_temp = 1 

var t1 = 0
var pos_vector = Vector2()
var pickable = true
var outside_screen = false 
var can_shoot = false 


func _ready():
	
	pos_vector = position #read initial enemy position as a starting point for sin 
	in_block = true
	break_away = false 
	aim_player = false 
	speed_block = 100 

func _physics_process(delta):
	
	harmonic_movement(delta)
	
	if in_block and not(aim_player) or outside_screen:
		move_block(delta)
		
#	if break_away and not(aim_player):
#		$Area2D.break_away = break_away
#		pickable = false 


func move_block(delta):

	move_dir = Global.block_move_dir
	
	position.x = pos_vector.x 
	position.y = pos_vector.y

func harmonic_movement(delta):

	move_dir = Global.block_move_dir
	step_x = (T*0.5)/2 #calculate increment at x up to pi/2
	h = step_x/N #x step
	arg += PI/2/N

	if pos_vector.x > 1620:
		Global.block_move_dir *= -1 
		pos_vector.x -= 10 
	elif pos_vector.x < 300:
		Global.block_move_dir *= -1 
		pos_vector.x += 10 

	pos_vector.x += (h * move_dir * delta)*speed_block 
	pos_vector.y += (cos(arg) * delta)*speed_block
	
#func _on_GunTimer_timeout():
#	can_shoot = true
	

	

	
	
	