extends CanvasLayer

var score = 0

func _ready():
	pass

func _process(delta):
	update_score()

func update_score():
	score = Global.score
	$Score.text = str(score) 