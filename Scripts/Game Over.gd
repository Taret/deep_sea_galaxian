extends Control
var back_button_finished = false 
var _timer 

func _ready():
	$GameOverSound.play()
	_timer = $BackButtonTimer
	_timer.wait_time = 0.1
	$Label2.text = str(Global.score)
	
func _process(delta):
	if back_button_finished:
		Global.player_lives = 3 
		Global.score = 0
		get_tree().change_scene("res://Scenes/Main.tscn")


func _on_TextureButton_pressed():
	$ButtonClicked.play()
	_timer.start()


func _on_BackButtonTimer_timeout():
	back_button_finished = true 


func _on_TextureButton_mouse_entered():
	$ButtonHovered.play()
