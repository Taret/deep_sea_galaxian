extends Control
var event
var start_button_finished = false 
var _timer 
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	_timer = $PlayButtonTimer
	_timer.wait_time = 0.1

func _process(delta):
	if start_button_finished:
		get_tree().change_scene("res://Scenes/Main.tscn")


func _on_StartButton_pressed():
	$ButtonClicked.play()
	_timer.start()
	

func _on_QuitButton_pressed():
	$ButtonClicked.play()
	get_tree().quit()
	


func _on_ControlsButton_pressed():
	$Buttons.hide()
	$Buttons2.show()
	$ButtonClicked.play()
	


func _input(event):
	if event is InputEventKey and event.pressed:
		$TextureRect2.hide() 
		$TextureRect.show()
		$Buttons.show()


func _on_BackButton_pressed():
	$Buttons2.hide()
	$Buttons.show()
	$ButtonClicked.play()
	
func play_ButtonHovered():
	$ButtonHovered.play()

func _on_PlayButtonTimer_timeout():
	start_button_finished = true 
	

func _on_StartButton_mouse_entered():
	play_ButtonHovered()


func _on_SoundButton_mouse_entered():
	play_ButtonHovered()
	

func _on_ControlsButton_mouse_entered():
	play_ButtonHovered()


func _on_QuitButton_mouse_entered():
	play_ButtonHovered()


func _on_BackButton_mouse_entered():
	play_ButtonHovered()


func _on_SoundButton_pressed():
	$ButtonClicked.play()
