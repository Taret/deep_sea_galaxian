extends Area2D


var h
var arg = 0
var curr_pos
var amp = 5
var move_dir

var speed_block = 100 
var T = 200
var N = 20 
var step_x 

var in_block
var aim_player 
var break_away 
var keep_block_pos = false

func _ready():
	pass



func _physics_process(delta):
	
	if keep_block_pos:
		move_block(delta)
		
	if break_away and not(aim_player):
		$Area2D.break_away = break_away


func move_block(delta):
	
	step_x = (T*0.5)/2 #calculate increment at x up to pi/2
	h = step_x/N #x step
	arg += PI/2/N
	
	curr_pos = position
	
	move_dir = Global.block_move_dir

	position.x += (h * move_dir * delta)*speed_block
	position.y = curr_pos.y + (cos(arg) * delta)*speed_block
	
	

