extends Node

var play_glass = false
var destroyed_enemies = 0 
var pollution = 0 

func _ready():
	Global.score = 0 
	pass
	

func _process(delta):
	
	#print(destroyed_enemies)
	
	
	check_pollution(pollution)
	
	if destroyed_enemies == 5:
		get_tree().change_scene("res://Scenes/Winner.tscn")
		
		
	if pollution == 3:
		Global.player_lives = 0 
	
	
	if Global.player_lives == 0:
		$CanvasLayer/Glass3.show()
		$CanvasLayer/Glass2.hide()
	
	
	if Global.player_lives == 2:
		$CanvasLayer/Glass1.show()
		
	if Global.player_lives == 1:
		$CanvasLayer/Glass2.show()
		$CanvasLayer/Glass1.hide()
		
		
		
func check_pollution(pollution):
	if pollution == 1:
		$CanvasLayer/GUI/Toxic1.show()
	
	if pollution == 2:
		$CanvasLayer/GUI/Toxic2.show()
		
	if pollution == 3:
		$CanvasLayer/GUI/Toxic3.show()
	

func Play_Barrel_Catched():
	get_node("Sounds/Barrel_catched").play()

func _on_Player_shoot(bullet, pos, dir):
	var b = bullet.instance()
	b.start(pos,dir)
	get_node("CanvasLayer").add_child(b)
	get_node("Sounds/Shoot").play()

func _on_Shoot_finished():
	get_node("Sounds/Shoot").stop()

func play_explode_sound():
	get_node("Sounds/Enemy_explosion").play()
	
func play_glass_sound():
	get_node("Sounds/Glass_cracking").play()
	play_glass = true
	
func _on_Glass_cracking_finished():
	get_node("Sounds/Glass_cracking").stop()

func _on_Glass1_visibility_changed():
	play_glass_sound()

func _on_Glass2_visibility_changed():
	play_glass_sound()
	
func play_enemy_shoot_sound():
	get_node("Sounds/Enemy_shoot").play()
	
func play_break_away_sound():
	get_node("Sounds/Enemy1_break_away").play()
	
func Play_Ship_Explosion():
	get_node("Sounds/Player_explosion").play()
	
func Play_Enemy_Barrel_Sound():
	get_node("Sounds/Enemy_barrel_shoot").play()


func _on_Glass3_visibility_changed():
	play_glass_sound()
	
func Play_barrel_explosion():
	get_node("Sounds/Barrel_explosion").play()
