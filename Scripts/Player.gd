extends RigidBody2D
enum {INIT, ALIVE, INVULNERABLE, DEAD}
var state = null 
export (int) var SPEED

var screensize = Vector2()
var motion = Vector2()

signal shoot
export (PackedScene) var Bullet 
export (float) var fire_rate 
var can_shoot = true

export var path_amount = 50

var took_dmg = false 
var can_take_dmg = true 
var alive = true 
var death_timer_triggered = true
var floodgate_open = false 

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#change_state(ALIVE)
	screensize = get_viewport().get_visible_rect().size #reads screensize
	$GunTimer.wait_time = fire_rate

func _process(delta):

	if took_dmg:
		can_take_dmg = false
	
	
	set_physics_process(true)
	get_input()	
	check_if_alive()
	check_quit_game()
	Global.player_pos = global_position
	
	if took_dmg:
		flicker()
	
	
func check_quit_game():
	if Input.is_action_pressed("end_game"):
		get_tree().quit()

func check_if_alive():
	if Global.player_lives == 0:
		alive = false 
		
		
		if death_timer_triggered:
			$Bubbles.hide()
			get_tree().get_root().get_node("Main").Play_Ship_Explosion()
			print("Score: ", Global.score)
			$AnimatedSprite.play("explode")
			death_timer_triggered = false 
			$DeathTimer.start()
			
func finish_game():
	get_tree().change_scene("res://Scenes/Game Over.tscn")

func get_input():
	
	if alive:
		motion.x = 0
		motion.y = 0 
	
	check_floodgate()	
	
	
	if can_shoot:
		if floodgate_open and alive:	
	
			if Input.is_action_pressed("ui_right") and !(Input.is_action_pressed("ui_left")):
				motion.x = SPEED 
				$AnimatedSprite.play("right-floodgate-active")
				print("TTUTUTU")
				
				if global_position.x > 1600:
					motion.x = 1
				
			elif Input.is_action_pressed("ui_left") and !(Input.is_action_pressed("ui_right")):
				motion.x = -SPEED
				$AnimatedSprite.play("left-floodgate-active")
				if global_position.x < 320:
					motion.x = -1
				
			else:
				motion.x = 0
				$AnimatedSprite.play("idle-floodgate-active")
				
			if Input.is_action_pressed("shoot") and can_shoot and not(floodgate_open):
				shoot()
		
		elif not(floodgate_open) and alive:
			
			if Input.is_action_pressed("ui_right") and !(Input.is_action_pressed("ui_left")):
				motion.x = SPEED 
				$AnimatedSprite.play("right-active")
				
				if global_position.x > 1600:
					motion.x = 1
				
			elif Input.is_action_pressed("ui_left") and !(Input.is_action_pressed("ui_right")):
				motion.x = -SPEED
				$AnimatedSprite.play("left-active")
				if global_position.x < 320:
					motion.x = -1
				
			else:
				motion.x = 0
				$AnimatedSprite.play("idle-active")
				
			if Input.is_action_pressed("shoot") and can_shoot and not(floodgate_open):
				shoot()
		
		
	else:
		
		if floodgate_open and alive:	
		
			if Input.is_action_pressed("ui_right") and !(Input.is_action_pressed("ui_left")):
				motion.x = SPEED 
				$AnimatedSprite.play("right-floodgate")
				
				if global_position.x > 1600:
					motion.x = 1
				
			elif Input.is_action_pressed("ui_left") and !(Input.is_action_pressed("ui_right")):
				motion.x = -SPEED
				$AnimatedSprite.play("left-floodgate")
				if global_position.x < 320:
					motion.x = -1
				
			else:
				motion.x = 0
				$AnimatedSprite.play("idle-floodgate")
				
			if Input.is_action_pressed("shoot") and can_shoot and not(floodgate_open):
				shoot()
			
		elif not(floodgate_open) and alive:
			
			if Input.is_action_pressed("ui_right") and !(Input.is_action_pressed("ui_left")):
				motion.x = SPEED 
				$AnimatedSprite.play("right")
				
				if global_position.x > 1600:
					motion.x = 1
				
			elif Input.is_action_pressed("ui_left") and !(Input.is_action_pressed("ui_right")):
				motion.x = -SPEED
				$AnimatedSprite.play("left")
				if global_position.x < 320:
					motion.x = -1
				
			else:
				motion.x = 0
				$AnimatedSprite.play("idle")
				
			if Input.is_action_pressed("shoot") and can_shoot and not(floodgate_open):
				shoot()
		
		
		
func check_floodgate():
	if Input.is_action_pressed("flood_gate"):
		floodgate_open = true 
	else:
		floodgate_open = false 		



func _integrate_forces(physics_state):
	set_applied_force(motion)
	
	var xform = physics_state.get_transform()

	physics_state.set_transform(xform)

func shoot():
	if state == INVULNERABLE:
		return
	emit_signal("shoot", Bullet, $Muzzle.global_position, rotation)
	can_shoot = false
	$GunTimer.start()
		
func _on_GunTimer_timeout():
	can_shoot = true
	
#
func _physics_process(delta):
	get_parent().set_offset(get_parent().get_offset() + (path_amount * delta)) #in pixels
	
	
func flicker():
	for i in 6:
		$AnimatedSprite.self_modulate.a = 0.5
		yield(get_tree(), "idle_frame")
		$AnimatedSprite.self_modulate.a = 1.0
		yield(get_tree(), "idle_frame")
	took_dmg = false
	$DamageTimer.start()


func _on_DeathTimer_timeout():
	finish_game()


func _on_DamageTimer_timeout():
	can_take_dmg = true 
