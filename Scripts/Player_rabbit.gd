#movement z kroliczka
extends KinematicBody2D

var motion = Vector2()
export var SPEED = 10
var screensize = Vector2()

func _ready():
	screensize = get_viewport().get_visible_rect().size #reads screensize

func _physics_process(delta):
	if Input.is_action_pressed("ui_right") and !(Input.is_action_pressed("ui_left")):
		motion.x = SPEED
	elif Input.is_action_pressed("ui_left") and !(Input.is_action_pressed("ui_right")):
		motion.x = -SPEED
	else:
		motion.x = 0 

	move_and_collide(motion)