extends Sprite
var show_texture = true
var hide_texture = false 

func _ready():
	$TimerOn.wait_time = 0.5
	$TimerOff.wait_time = 0.5
	
	$TimerOn.start()

func _process(delta):
	pass


func _on_TimerOn_timeout():
	show()
	$TimerOff.start()

func _on_TimerOff_timeout():
	hide()
	$TimerOn.start()
	
