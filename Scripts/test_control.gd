extends Node2D

var can_pick = true 
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$Timer.wait_time = 2

func _process(delta):
	pick_enemy()

func _on_Timer_timeout():
	can_pick = true

func pick_enemy():
	if can_pick: 
	
		randomize()
		var id = randi() % get_child_count() - 2
		if id < 0:
			id *= -1
		
		var enemy = get_child(id)
		print(id)
		
		$Timer.start()
		enemy.aim_player = true
		enemy.in_block = false
		can_pick = false 
		
		
		
		
	
