extends CanvasLayer
var label
var modulate = false 

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	label = $text1
	

	$Timer.start()
	pass

func _process(delta):
	if modulate:
		modulate_a(label, delta)


func _on_Timer_timeout():
	modulate = true 
	

func modulate_a(label, delta):
	label.self_modulate.a += 0.09 * delta