extends Area2D

var curr_pos
var move_dir
var x = 0
var h
var T = 200 
var N = 20
var step_x
var arg = 0

var is_alive = true

var target = Vector2()
var velocity = Vector2()
var speed = 50
var aim_player = false 

var in_block = true 


func _ready():
	pass

func _physics_process(delta):
	
	if in_block:
		move_block(delta)
		
	if aim_player:
		move_to_player(Vector2(970,600), delta)

func move_block(delta):
	step_x = (T*0.5)/2 #calculate increment at x up to pi/2
	h = step_x/N #x step
	arg += PI/2/N
	#
	curr_pos = position
	move_dir = Global.block_move_dir
	#
	position.x += (h * move_dir * delta)*speed
	position.y = curr_pos.y + (cos(arg) * delta)*speed
	
func move_to_player(target, delta):
	var direction = (target - position).normalized()
	var motion = direction * speed * delta
	position += motion
	


#func _process(delta):
#
#	if can_pick_enemy:
#		pick_enem()
#
#
#func _on_EnemyTimer_timeout():
#	can_pick_enemy = true
#
#func pick_enem():
#	randomize()
#
#	var enemies = get_child_count()
#	print("Enemies: " , enemies)
#	if enemies > 0:
#		var enem_id = randi() % enemies - 1 #Muzzle last one 
#		print("id: ", enem_id)
#		get_child(enem_id).in_block = false 
#		can_pick_enemy = false 
#
#		$EnemyTimer.start()


